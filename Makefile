# sfftv - simple fast fourier transform viewer

include config.mk

SRC = sfftv.c drw.c fft.c serial.c util.c
OBJ = ${SRC:.c=.o}

all: options sfftv

options:
	@echo sfftv build options:
	@echo "CFLAGS   = ${CFLAGS}"
	@echo "LDFLAGS  = ${LDFLAGS}"
	@echo "CC       = ${CC}"

.c.o:
	${CC} -c ${CFLAGS} $<

${OBJ}: config.h config.mk

sfftv: ${OBJ}
	${CC} -o $@ ${OBJ} ${LDFLAGS}

clean:
	rm -f sfftv ${OBJ} sfftv-${VERSION}.tar.xz

dist: clean
	mkdir -p sfftv-${VERSION}
	cp -R ${SRC} config.h\
		arg.h drw.h fft.h serial.h util.c\
		Makefile config.mk LICENSE README sfftv.1\
		\sfftv-${VERSION}
	tar -cf sfftv-${VERSION}.tar sfftv-${VERSION}
	xz -9 -T0 sfftv-${VERSION}.tar
	rm -rf sfftv-${VERSION}

install: all
	mkdir -p ${PREFIX}/bin
	cp -f sfftv ${PREFIX}/bin
	chmod 755 ${PREFIX}/bin/sfftv
	mkdir -p ${MANPREFIX}/man1
	sed "s/VERSION/${VERSION}/g" < sfftv.1 > ${MANPREFIX}/man1/sfftv.1
	chmod 644 ${MANPREFIX}/man1/reichspost.1

uninstall:
	rm -r ${PREFIX}/bin/sfftv\
		${MANPREFIX}/man1/sfftv.1

.PHONY: all options clean dist install uninstall
