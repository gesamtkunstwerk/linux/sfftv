#include <ncurses.h>

#include "drw.h"

/* Functions implementations */
void
drw_char(int x, int y,char c)
{
	if( (COL<x) && ((LINE-2)<y) )
		return;
	mvaddch(y,x,c);
	refresh();
}

void
drw_clean()
{
	for( int i=0 ; i<(LINE-1) ; i++ )
	{
		move(i,0);
		for( int j = 0 ; j<=COL ; j++ )
			addch(' ');
	}

	move(LINE-1,0);
	for( int i=0 ; i<=COL ; i++)
		addch('=');

	mvaddch(LINE,0,'>');
	for( int i=1 ; i<=COL ; i++)
		addch(' ');

	refresh();
}

void
drw_cleaninput()
{
	mvaddch(LINE,0,'>');
	for(int i=1 ; i<=COL ; i++ )
		mvaddch(LINE,i,' ');
}

void
drw_end()
{
	endwin();
}

void
drw_input(char *mesg, int mod)
{
	switch(mod)
	{
	case 0:
		move(LINE,1);
		break;
	case 1:
		move(LINE,5);
		addch('>');
		break;
	case 2:
		move(LINE,10);
		addch('>');
		break;
	default:
		return;
	}

	for( int i=0 ; i<5 ; i++)
	{
		mesg[i] = getch();
		if(mesg[i]=='\n')
		{
			mesg[i]='\0';
			break;
		}
		addch(mesg[i]);
		refresh();
	}
}

void
drw_start()
{
	/* Init and configuration */
	initscr();		/* Start curses mode       */
	raw();			/* Line buffering disabled */
	noecho();		/* Dont echo               */
	curs_set(0);		/* Hide cursor             */
	keypad(stdscr, TRUE);	/* Fx enable               */

	drw_clean();
}
