#include <ncurses.h>

/* Variables */
#define	COL	COLS-1
#define	LINE	LINES-1

/* Functions declarations */
void drw_char(int x, int y, char c);
void drw_clean(void);
void drw_cleaninput(void);
void drw_end(void);
void drw_input(char *mesg, int mod);
void drw_start(void);

