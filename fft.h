#include <complex.h>

#define PI 3.14159265358979323846

/* Functions declarations */
float fft_getconst(int x);
void fft_reset();
int fft_update(float x);

