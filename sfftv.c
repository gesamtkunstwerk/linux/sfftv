#include <math.h>
#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

char*argv0;
#include "arg.h"
#include "drw.h"
#include "fft.h"
#include "serial.h"
#include "util.h"

#include "config.h"

typedef struct DataPort DataPort;
struct DataPort {
	char mess[18];
	int ready;
	int typesign;
	int typemeas;
	int typedata;
};

/* Functions declarations */
static void drawdata(int max);
static void getfft(int x);
static void getmeas(int f);
static void getsignal(int x);
static void *listenport(void *args);
static void requestdata(void);
static void usage(void);

/* Globals */
static char *serialdevice = NULL;
static int serialbaud = 0;
static int datain[N];
static int dataout[N];
static DataPort dataport;

/* Functions implementations */
void
drawdata(int max)
{
	int x,yi,yo;
	float xm,ym;

	if( (max<=10) || (N<max) )
		return;

	/* Ajust to Windows size */
	xm = max/(float)(COL);
	ym = (LINE-2)/(float)(MAX_ADC);

	/* Draw */
	for( int i = 0 ; i<=COL ; i++ )
	{
		x = i*xm;
		yi = LINE - 2 - (datain[x]*ym);
		yo = LINE - 2 - (dataout[x]*ym);

		if(yi==yo)
		{
			drw_char(i,yi,'*');
			continue;
		}
		drw_char(i,yi,'+');
		drw_char(i,yo,'o');
	}
}

void
getfft(int x)
{
	/* Clean windows */
	drw_clean();
	/* fft input */
	fft_reset();
	for( int i=0 ; i<N ; i++)
		fft_update(datain[i]);

	for( int i=0 ; i<N ; i++)
		datain[i] =  2*fft_getconst(i);
	/* fft output */
	fft_reset();
	for( int i=0 ; i<N ; i++)
		fft_update(dataout[i]);

	for( int i=0 ; i<N ; i++)
		dataout[i] =  2*fft_getconst(i);
	drawdata(x);
}

void
getmeas(int f)
{
	char c;
	char mess[6];

	float ym;
	int yo;

	ym = (LINE-5)/(float)(MAX_ADC);

	/* Clear Windows*/
	drw_clean();
	drw_char(1,1,'m');
	drw_char(2,1,'<');
	drw_char(7,1,'>');
	drw_char(8,1,':');
	drw_char(16,1,'H');
	drw_char(24,1,'U');

	for( int i=0 ; i<COL ; i++ )
	{
		/* Print COL */
		sprintf(mess,"%4d",i%10000);
		for( int j=0 ; j<4 ; j++ )
			drw_char(3+j,1,mess[j]);
		memset(&mess[0],0,sizeof(mess));
		/* Send Command */
		c='m';
		dataport.ready = 0;
		dataport.typemeas = 0;
		while(serial_writechar(&c,sizeof(c)))
			system("sleep 1e-3");
		/* Get data */
		while(!dataport.typemeas)
			system("sleep 1e-3");

		if( dataport.mess[5] != 'f' )
		{
			i--;
			continue;
		}
		dataport.mess[5] = '\0';
		yo = atoi(dataport.mess);

		/* Display data: hex*/
		sprintf(mess,"%5d",yo);

		for( int j=0 ; j<5 ; j++ )
			drw_char(10+j,1,mess[j]);
		/* Display data: Unit */
		sprintf(mess,"%5d", (int)(yo*f/(float)65535) );
		for( int j=0 ; j<5 ; j++ )
			drw_char(18+j,1,mess[j]);
		/* Draw graphic */
		yo = LINE - 2 - (yo*ym);
		drw_char(i,yo,'*');

		/* Wait 0.5s */
		system("sleep 0.5");
	}
	dataport.ready = 0;
	dataport.typemeas = 0;
}

void
getsignal(int x)
{
	char mess[5];

	if( (x<=0) || ((N/2)<x) )
		return;

	memset(&mess[0],0,sizeof(mess));
	sprintf(mess,"s%3d", (N/(2*x))%1000 );

	/* Clear data */
	for( int i=0 ; i<N ; i++ )
	{
		datain[i] = 0;
		dataout[i] = 0;
	}
	/* Clean windows */
	drw_clean();
	/* Send fft command to microcontroller */
	while(serial_writechar(mess,sizeof(mess)))
		system("sleep 1e-3");
	/* Wait confirm to microcontroller */
	while(!dataport.typesign)
		system("sleep 1e-3");
	dataport.ready = 0;
	dataport.typesign = 0;
	/* Get data */
	requestdata();
	/* Draw data */
	drawdata(N);
}

void *
listenport(void *args)
{
	char c;

	dataport.ready = 0;
	dataport.typesign = 0;
	dataport.typemeas = 0;
	dataport.typedata = 0;

	while(1)
	{
		c='\0';
		memset(&dataport.mess[0],0,sizeof(dataport.mess));

		while(serial_readchar(&c,sizeof(c)));

		switch(c)
		{
		case 'S':
			dataport.ready = 1;
			dataport.typesign = 1;
			break;
		case 'd':
			for( int i=0 ; i<6 ; i++ )
			{
				while(serial_readchar(&c,sizeof(c)));
				dataport.mess[i] = c;
			}
			dataport.ready = 1;
			dataport.typemeas = 1;
			break;
		case 'D':
			for( int i=0 ; i<18 ; i++ )
			{
				while(serial_readchar(&c,sizeof(c)));
				dataport.mess[i] = c;
			}
			dataport.ready = 1;
			dataport.typedata = 1;
			break;
		default:
			break;
		}

		while(dataport.ready||dataport.typesign||
			dataport.typemeas||dataport.typedata)
			system("sleep 1e-5");
	}
	pthread_exit(NULL);
}

void
requestdata()
{
	char mess[7];

	for( int i=0 ; i<N ; i++)
	{
		/* Send get data of fft command to microcontroller */
		sprintf(mess,"d%4d",i%N);
		for( int j=0 ; j<5 ; j++ )
			drw_char(j,0,mess[j]);
		/* Get data */
		dataport.ready = 0;
		dataport.typedata = 0;

		while(serial_writechar(mess,sizeof(mess)))
			system("sleep 1e-4");

		while(!dataport.typedata)
			system("sleep 1e-4");

		if( (dataport.mess[0]!='p') || (dataport.mess[5]!='i') ||
			(dataport.mess[11]!='o') || (dataport.mess[17]!='f') )
		{
			i--;
			continue;
		}
		/* Check data */
		memset(&mess[0],0,sizeof(mess));
		for( int j=0 ; j<4 ; j++ )
			mess[j] = dataport.mess[1+j];
		if( atoi(mess) != i )
		{
			i--;
			continue;
		}
		/* Save data */
		memset(&mess[0],0,sizeof(mess));
		for( int j=0 ; j<5 ; j++ )
			mess[j] = dataport.mess[6+j];
		datain[i] = atoi(mess);
		for( int j=0 ; j<5 ; j++ )
			mess[j] = dataport.mess[12+j];
		dataout[i] = atoi(mess);
	}
	dataport.ready = 0;
	dataport.typedata = 0;
}

void
usage()
{
	die(	"usage: sfftv [OPTION]\n\n"
		"  -b        Baudrate\n"
		"  -d        Device\n"
		"  -v        Show version\n"
		"  -h        Help command\n" );
}

int
main( int argc, char *argv[] )
{
	/* Variables */
	char com[5];
	pthread_t thread_listen;

	/* Arguments */
	ARGBEGIN {
	case 'b':
		serialbaud = atoi(EARGF(usage()));
		break;
	case 'd':
		serialdevice = EARGF(usage());
		break;
	case 'v':
		die("sfftv-"VERSION"\n");
		return 0;
	default:
		usage();
	} ARGEND

	if(!serialbaud)
		serialbaud = 9600;

	if(!serialdevice)
	{
		printf("No device\n");
		return 1;
	}

	/* Open port */
	if( serial_openport(serialdevice, serialbaud) != 0 )
	{
		printf("%s no connect\n",serialdevice);
		return 1;
	}

	/* Thread -> listen to serial port */
	if(pthread_create(&thread_listen, NULL, listenport, NULL))
		die("error to create a thread for listen\n");

	/* Start draw */
	drw_start();

	while(1)
	{
		/* Wait for command */
		drw_cleaninput();
		serial_clean();
		drw_input(com,0);

		if( !strcmp(com,"fft") )
		{
			drw_input(com,1);
			if( !strcmp(com,"t") )
				getfft((N/2) - 1);
			if( !strcmp(com,"w") )
				getfft(COL);
			if( !strcmp(com,"m") )
			{
				drw_input(com,2);
				getfft(atoi(com));
			}
		}
		if( !strcmp(com,"mea") )
		{
			drw_input(com,1);
			getmeas(atoi(com));
		}
		if( !strcmp(com,"sig") )
		{
			drw_input(com,1);
			getsignal(atoi(com));
		}
		if( !strcmp(com,"exit") )
			break;
	}

	/* Close draw*/
	drw_end();
	/* Kill all threads */
	pthread_kill(thread_listen, SIGKILL);

	return 0;
}
