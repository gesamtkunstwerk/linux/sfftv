#include <complex.h>

#include "fft.h"

#include "config.h"

/* Globals */
static float complex g[N/2];
static float complex h[N/2];

static float fftmid;
static int fftcount;

/* Functions implementations */
float
fft_getconst(int x)
{
	float complex tem;

	if( (x<0) || (N<=x) || (fftcount!=N) )
		return 0;

	tem = 0 + (-x*2*PI/N)*I;
	tem = cexp(tem);

	if(x==N/2)
		return cabs(g[x-N/2] - (h[x-N/2]*tem))/(N);
	if(x==0)
		return cabs(g[x] + (h[x]*tem))/(N);

	if(N/2 <= x)
		return cabs(g[x-N/2] - (h[x-N/2]*tem))/(N/2);
	else
		return cabs(g[x] + (h[x]*tem))/(N/2);
}

int
fft_update(float x)
{
	if(fftcount >= N)
		return 1;

	if(!(fftcount%2))
	{
		fftmid = x;
		fftcount++;
		return 0;
	}

	float complex Feh;

	for( int k=0 ; k<(N/2) ; k++ )
	{
		Feh = 0 + (-4*k*PI*((fftcount-1)/2)/N)*I;
		Feh = cexp(Feh);
		g[k] += fftmid*Feh;
		h[k] += x*Feh;
	}

	fftcount++;

	return 0;
}

void
fft_reset()
{
	fftcount = 0;
	fftmid = 0.0;

	for( int k=0 ; k<(N/2) ; k++ )
	{
		g[k] = 0+0*I;
		h[k] = 0+0*I;
	}
}
